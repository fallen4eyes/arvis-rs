use crate::runtime::visual::MyGame;
use crate::views::View;
use coffee::graphics::{Color, Frame, Point, Rectangle, Sprite};
use coffee::Timer;

fn draw_bars(controller: &mut MyGame, frame: &mut Frame, _timer: &Timer) {
    frame.clear(Color::BLACK);
    let window_height = frame.height();
    let window_width = frame.width();

    controller.renderbatch.clear();
    for (max, set) in controller.arr.arrays.iter().take(1) {
        let colors = &controller.arr.colors;
        for i in 0..window_width as usize {
            let x = ((i as f32 / window_width) * set.len() as f32) as usize;
            let val = set[x];
            let height = window_height * (val as f32 / set.len() as f32);

            let color = match colors.get(&0).unwrap().get(&x) {
                Some(n) => *n as u16,
                None => 0,
            };
            controller.renderbatch.add(Sprite {
                source: Rectangle {
                    x: color,
                    y: 0,
                    width: 1,
                    height: 1,
                },
                position: Point::new(i as f32, window_height - height),
                scale: (1.0, height),
            });
        }
    }
    controller.renderbatch.draw(&mut frame.as_target());
}

pub fn bars_view() -> View {
    View {
        name: "Vertical Bars",
        draw: draw_bars,
    }
}
