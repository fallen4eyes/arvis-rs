use crate::runtime::visual::MyGame;
use crate::views::View;
use coffee::graphics::{Color, Frame, Point, Rectangle, Sprite};
use coffee::Timer;

fn draw_bars(controller: &mut MyGame, frame: &mut Frame, _timer: &Timer) {
    frame.clear(Color::BLACK);
    let window_height = frame.height();
    let window_width = frame.width();

    controller.renderbatch.clear();
    let array_count = controller.arr.arrays.iter().filter(|x| x.1.len() >= 4).count();

    for (arr_index, set) in controller.arr.arrays.iter().filter(|x| x.1.len() >= 4).enumerate() {
        let colors = &controller.arr.colors;
        //let height_offset = (window_height / array_count as f32) * (array_count - arr_index) as f32;
        let height_offset = if arr_index == 0 {
            window_height
        } else {
            ((window_height / 2.0) / (array_count - 1) as f32) * (array_count - arr_index) as f32
        };
        for i in 0..window_width as usize {
            let x = ((i as f32 / window_width) * set.1.len() as f32) as usize;
            let val = set.1[x];
            //let height = (window_height * (val as f32 / set.1.max as f32)) / array_count as f32;
            let height = if arr_index == 0 {
                (window_height * (val as f32 / set.1.max as f32)) / 2.0
            } else {
                ((window_height * (val as f32 / set.1.max as f32)) / array_count as f32) / 2.0
            };

            let color = match colors.get(&set.0).unwrap().get(&x) {
                Some(n) => *n as u16,
                None => 0,
            };
            controller.renderbatch.add(Sprite {
                source: Rectangle {
                    x: color,
                    y: 0,
                    width: 1,
                    height: 1,
                },
                position: Point::new(i as f32, height_offset - height),
                scale: (1.0, height),
            });
        }
    }
    controller.renderbatch.draw(&mut frame.as_target());
}

pub fn aux_bars_view() -> View {
    View {
        name: "Aux-Aware Vertical Bars",
        draw: draw_bars,
    }
}
