use std::cmp::Ordering;
use crate::sorts::{Sortable, Algorithm};

pub fn bubble_sort<S: Sortable<usize>>(mut arr: S) {
    for e in (1..=arr.len()).rev() {
        for i in 1..e {
            if arr.cmp(i-1, i) == Ordering::Greater {
                arr.swap(i-1, i);
            }
        }
    }
}

pub fn bubble_sort_algorithm<S: Sortable<usize>>() -> Algorithm<S> {
    Algorithm {
        name: "bubble sort",
        base_size: 128,
        sort: bubble_sort,
    }
}
