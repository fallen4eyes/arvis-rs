use crate::sorts::{Sortable, Algorithm};

pub fn merge_sort<S: Sortable<usize>>(mut arr: S) {
    let len = arr.len();
    internal_merge_sort(&mut arr, 0, len);
}

fn internal_merge_sort<S: Sortable<usize>>(arr: &mut S, start: usize, end: usize) {
    if end - start >= 2 {
        let mid = start + ((end - start) / 2);
        internal_merge_sort(arr, start, mid);
        internal_merge_sort(arr, mid, end);
        let mut left = arr.empty(mid - start);
        for i in 0..(mid-start) {
            left.replace(i, *arr.get(i + start));
        }
        let mut right = arr.empty(end - mid);
        for i in 0..(end-mid) {
            right.replace(i, *arr.get(i + mid));
        }
        let merged = merge(left,right);
        for i in 0..merged.len() {
            arr.replace(start + i, *merged.get(i));
        }
    }
}

fn merge<S: Sortable<usize>>(left: S, right: S) -> S {
    let mut leftptr = 0;
    let mut rightptr = 0;
    let mut buffer = left.empty(left.len() + right.len());
    let mut buffer_offset = 0;

    while leftptr < left.len() && rightptr < right.len() {
        if left.get(leftptr) <= right.get(rightptr) {
            buffer.replace(buffer_offset, *left.get(leftptr));
            leftptr += 1;
        } else {
            buffer.replace(buffer_offset, *right.get(rightptr));
            rightptr += 1;
        }
        buffer_offset += 1;
    }

    while leftptr < left.len() {
        buffer.replace(buffer_offset, *left.get(leftptr));
        leftptr += 1;
        buffer_offset += 1;
    }

    while rightptr < right.len() {
        buffer.replace(buffer_offset, *right.get(rightptr));
        rightptr += 1;
        buffer_offset += 1;
    }

    buffer
}

pub fn merge_sort_algorithm<S: Sortable<usize>>() -> Algorithm<S> {
    Algorithm {
        name: "merge sort",
        base_size: 256,
        sort: merge_sort,
    }
}
