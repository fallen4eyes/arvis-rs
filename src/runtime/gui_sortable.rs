//runtime/gui_sortable.rs
//The GuiSortable struct, which acts as the basic algorithm container.
//Similar to a regular Vec collection,
//however, it holds state about its relation to other parts of an algorithm,
//as well as a sender channel to be used by the executor(sorter.rs).
use crate::sorts::Sortable;
use crate::data::Event;
use std::sync::mpsc::SyncSender;
use std::cmp::Ordering;
use std::sync::Arc;
use std::sync::atomic::AtomicUsize;
use std::sync::atomic::Ordering as SyncOrdering;
use std::thread;


#[derive(Clone)]
pub struct GuiSortable {
    pub arr: Vec<usize>,
    pub emit: SyncSender<Event>,
    pub global_id: Arc<AtomicUsize>,
    pub id: usize,
    pub main: bool,
}

impl GuiSortable {
    fn send(&self, event: Event) -> bool {
        matches!(self.emit.send(event), Ok(_))
    }
}

impl Sortable<usize> for GuiSortable {
    fn empty(&self, len: usize) -> Self {
        let id = self.global_id.fetch_add(1, SyncOrdering::Relaxed);
        self.send(Event::New{id, len});
        GuiSortable{ 
            arr: vec![0; len], 
            emit: self.emit.clone(), 
            global_id: Arc::clone(&self.global_id),
            id: id,
            main: false 
        }
    }

    fn cmp(&self, a: usize, b: usize) -> Ordering {
        if self.send(Event::Cmp{id: self.id, elements:(a, b)}) {
            self.arr[a].cmp(&self.arr[b])
        }
        else {
            Ordering::Equal
        }
    }

    fn swap(&mut self, a: usize, b: usize) {
        if self.send(Event::Swap{id: self.id, elements:(a,b)}) {
            self.arr.swap(a, b)
        }
    }

    fn get(&self, i: usize) -> &usize {
        if self.send(Event::Get{id: self.id, i}) {
            &self.arr[i]
        }
        else {
            &0
        }
    }

    fn replace(&mut self, i: usize, v: usize) {
        if self.send(Event::Replace{id: self.id, i, v}) {
            self.arr[i] = v;
        }
    }

    fn len(&self) -> usize { self.arr.len() }
}

impl Drop for GuiSortable {
    fn drop(&mut self) {
        if !thread::panicking() {
            self.send(Event::Drop{id: self.id});
        }
    }
}
