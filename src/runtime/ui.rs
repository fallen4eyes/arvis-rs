//runtime/ui.rs
//The User Interface abstraction for arvis.
//Defines the layout for built-in components,
//along with the system for user-control events.
//Operates in conjunction with the controller(visual.rs).

use crate::MyGame;
use coffee::graphics::{Color, HorizontalAlignment, Window};
use coffee::ui::{
    button, slider, Align, Button, Checkbox, Column, Element, Justify, Renderer, Row, Slider, Text,
    UserInterface,
};

#[derive(Debug, Clone, Copy)]
pub enum Message {
    SpeedChanged(f32),
    SizeChanged(f32),
    ToggleSort(usize, bool),
    ResetSorts,
    ResetSpeed,
}

fn uibar<'a>(
    delay: f32,
    speedslider: &'a mut slider::State,
    speed: f32,
    sizeslider: &'a mut slider::State,
    size: f32,
    resetbutton: &'a mut button::State,
    uibutton: &'a mut button::State,
    window: &Window,
) -> Element<'a, Message> {
    Column::new()
        .spacing(10)
        .justify_content(Justify::End)
        .align_items(Align::End)
        .push(
            Row::new()
                .width((window.width() / 2.0) as u32)
                .spacing(20)
                .padding(10)
                .push(Button::new(uibutton, "Reset Speed").on_press(Message::ResetSpeed))
                .push(
                    Column::new()
                        .push(
                            Text::new(&format!("Speed Factor: {:.3}ms", delay))
                                .horizontal_alignment(HorizontalAlignment::Center),
                        )
                        .push(Slider::new(
                            speedslider,
                            -2.0..=2.0,
                            speed,
                            Message::SpeedChanged,
                        )),
                )
                .push(
                    Column::new()
                        .push(
                            Text::new(&format!("Size Factor: {}", size as u32))
                                .horizontal_alignment(HorizontalAlignment::Center),
                        )
                        .push(Slider::new(sizeslider, 1.0..=256.0, size, move |size| {
                            Message::SizeChanged(size)
                        })),
                )
                .push(Button::new(resetbutton, "Reset Sorts").on_press(Message::ResetSorts)),
        )
        .into()
}

impl UserInterface for MyGame {
    type Message = Message;
    type Renderer = Renderer;

    fn react(&mut self, msg: Message, _window: &mut Window) {
        match msg {
            Message::SpeedChanged(speed) => {
                self.update_time = ((1000.0 / 60.0)as f32).powf(speed);
                self.delay_power = speed;
            }
            Message::SizeChanged(size) => {
                self.size_factor = size;
            }
            Message::ResetSorts => {
                for (c, k) in self.arr.midi.iter() {
                    self.audio.synth.noteoff(*c, *k);
                }
                self.currentsort = 0;
                self.arr.sort(
                    self.sorts[self.currentsort].base_size * self.size_factor as usize,
                    self.selected_distribution,
                    self.sorts[self.currentsort].clone()
                );
                self.waiting = false;
            }
            Message::ResetSpeed => {
                self.update_time = 1000.0 / 60.0;
                self.delay_power = 1.0;
            }
            Message::ToggleSort(i, state) => {
                
            }
        }
    }

    fn layout(&mut self, window: &Window) -> Element<Message> {
        let mut ui = Column::new()
            .width(window.width() as u32)
            .height(window.height() as u32)
            .justify_content(Justify::SpaceBetween)
            .padding(10);
        let mut top = Row::new()
            .width(window.width() as u32)
            .justify_content(Justify::SpaceBetween)
            .padding(10)
            .push(
                Text::new(&self.arr.info.gen_info(self.arr.size, self.update_time as f32 / self.arr.base_rate as f32)).height(window.height() as u32),
            );
        if self.showing_ui {
            top = top.push(uibar(
                self.update_time,
                &mut self.speed_slider,
                self.delay_power,
                &mut self.size_slider,
                self.size_factor,
                &mut self.reset_sort,
                &mut self.reset_speed,
                window,
            ));
            ui = ui.push(top);
        }
        /*if self.showing_sorts {
            let mut sorts = Column::new().spacing(10);
            let mut temp = Row::new().justify_content(Justify::SpaceBetween);
            for i in 0..self.selector.len() {
                if i % 4 == 0 && i != 0 {
                    sorts = sorts.push(temp);
                    temp = Row::new();
                }
                let value = self.selector[i].enabled;
                temp = temp.push(
                    Checkbox::new(
                        self.selector[i].enabled,
                        &self.selector[i].name,
                        move |_| Message::ToggleSort(i, !value),
                    )
                    .label_color(Color::BLACK),
                );
            }
            if self.selector.len() % 4 != 0 {
                sorts = sorts.push(temp);
            }
            ui = ui.push(sorts);
        }*/
        ui.into()
    }
}
